﻿using UnityEngine;
using System.Collections;

public class TargetsGenerator : MonoBehaviour 
{
	public GameObject targetCubePrefab;

	void Start () 
	{
		InvokeRepeating ("CreateTarget", 0, 1f);
	}

	void CreateTarget()
	{
		Vector3 direction = Random.onUnitSphere;
		direction.y = Mathf.Clamp(direction.y, 0.1f, 1f);
		float distance = 2 * Random.value + 1.5f;
		Instantiate (targetCubePrefab, direction*distance, Quaternion.identity);
	}
}
